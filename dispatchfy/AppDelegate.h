//
//  AppDelegate.h
//  dispatchfy
//
//  Created by Tatsuya Kaneko on 06/11/15.
//  Copyright © 2015 Tatsuya Kaneko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

