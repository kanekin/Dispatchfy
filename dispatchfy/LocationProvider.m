//
//  LocationProvider.m
//  dispatchfy
//
//  Created by Tatsuya Kaneko on 06/11/15.
//  Copyright © 2015 Tatsuya Kaneko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationProvider.h"

@implementation LocationProvider{
    
    CLLocationManager* locationManager;
}


-(id) init{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    
    locationManager.pausesLocationUpdatesAutomatically = NO;
    [self requestPermission];
    [locationManager startUpdatingLocation];
    return self;
}

- (CLAuthorizationStatus) permissionState {
    return [CLLocationManager authorizationStatus];
}

- (void) requestPermission {
    // for iOS versions where LocationManagre has requestAlwaysAuthorization(iOS8 or higher)
    if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        // check if we haven't already asked permissions
        if (!([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
            // request the permissions
            [locationManager performSelectorOnMainThread:@selector(requestAlwaysAuthorization) withObject:nil waitUntilDone:YES];
        }
    }
    // for iOS versions where LocationManagre has the property allowsBackgroundLocationUpdates(iOS9 or higher)
    NSArray* backgroundModes  = [[NSBundle mainBundle].infoDictionary objectForKey:@"UIBackgroundModes"];
    
    if(backgroundModes && [backgroundModes containsObject:@"location"]) {
        if ([locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)]){
            [locationManager setAllowsBackgroundLocationUpdates:YES];
        }
    }
}

@end