//
//  LocationProvider.h
//  dispatchfy
//
//  Created by Tatsuya Kaneko on 06/11/15.
//  Copyright © 2015 Tatsuya Kaneko. All rights reserved.
//

#ifndef LocationProvider_h
#define LocationProvider_h
#import <CoreLocation/CoreLocation.h>

#endif /* LocationProvider_h */

@interface LocationProvider : NSObject <CLLocationManagerDelegate>{
}


- (CLAuthorizationStatus) permissionState;

- (void) requestPermission;
@end