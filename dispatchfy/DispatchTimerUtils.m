//
//  DispatchTimerUtils.m
//  dispatchfy
//
//  Created by Tatsuya Kaneko on 07/11/15.
//  Copyright © 2015 Tatsuya Kaneko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DispatchTimerUtils.h"

@implementation DispatchTimerUtils{
    
}

+ (dispatch_source_t) createDispatchTimer:(NSDate*) time withQueue: (dispatch_queue_t) queue andLeeway: (uint64_t)leeway andBlock: (void(^)()) block {
    // Adding extra delay for avoiding too early trigger due to leeway
    dispatch_time_t popTime = dispatch_walltime(DISPATCH_TIME_NOW, (int64_t)([time timeIntervalSinceNow] * NSEC_PER_SEC));
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer)
    {
        dispatch_source_set_timer(timer, popTime, DISPATCH_TIME_FOREVER, leeway);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}

+ (dispatch_source_t) createDispatchTimerWithNonWallTime:(NSDate*) time withQueue: (dispatch_queue_t) queue andLeeway: (uint64_t)leeway andBlock: (void(^)()) block {
    // Adding extra delay for avoiding too early trigger due to leeway
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)([time timeIntervalSinceNow] * NSEC_PER_SEC));
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer)
    {
        dispatch_source_set_timer(timer, popTime, DISPATCH_TIME_FOREVER, leeway);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}


+ (void) dispatchAfterWithWallTime:(NSDate*) time withQueue: (dispatch_queue_t) queue andBlock: (void(^)()) block{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)([time timeIntervalSinceNow] * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(),block);
}

+ (void) dispatchAfterWithNonWallTime:(NSDate*) time withQueue: (dispatch_queue_t) queue andBlock: (void(^)()) block{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)([time timeIntervalSinceNow] * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(),block);
}
@end
