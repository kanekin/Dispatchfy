//
//  ViewController.m
//  dispatchfy
//
//  Created by Tatsuya Kaneko on 06/11/15.
//  Copyright © 2015 Tatsuya Kaneko. All rights reserved.
//

#import "ViewController.h"
#import "LocationProvider.h"
#import "DispatchTimerUtils.h"

@interface ViewController ()

@end

@implementation ViewController

dispatch_source_t nonwallGCD;
dispatch_source_t wallGCD;
NSDate* scheduledTime;
LocationProvider* locationProvider;
bool useNSTimer = true;
NSTimer* nsTimer;

- (void)viewDidLoad {
    [super viewDidLoad];
    scheduledTime = [[NSDate alloc] initWithTimeIntervalSince1970:0];
    [self redirectNSLogToDocuments];
    locationProvider = [[LocationProvider alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)redirectNSLogToDocuments
{
    NSArray *allPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [allPaths objectAtIndex:0];
    NSString *pathForLog = [documentsDirectory stringByAppendingPathComponent:@"Dispatchfy.txt"];
    
    freopen([pathForLog cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
}

- (IBAction) showLocationPermission {
    
    NSString* message = [[NSString alloc] initWithFormat:@"Permission: %d", [locationProvider permissionState]];
    UIAlertView *eventAlert = [[UIAlertView alloc]
                               initWithTitle:@"Location permission status" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display the Hello World Message
    [eventAlert show];
}

- (IBAction) showScheduledTime {
    NSString* message;
    if (useNSTimer) {
        message = [[NSString alloc] initWithFormat:@"is Timer vaild?: %d", [nsTimer isValid]];
    }else{
        message = [[NSString alloc] initWithFormat:@"ScheduledTime: %@", scheduledTime];
    }
    UIAlertView *eventAlert = [[UIAlertView alloc]
                               initWithTitle:@"Location permission status" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // Display the Hello World Message
    [eventAlert show];
}

- (IBAction) startDipatchTimer {
    if (useNSTimer) {
        nsTimer = [self startNSTimer:10];
    }else{
        NSDate* now = [NSDate date];
        scheduledTime = [now dateByAddingTimeInterval:10*60*60];
        [self scheduleEventWithWallTime: scheduledTime];
        [self scheduleEventWithNonWallTime: scheduledTime];
        NSLog(@"[startDispatchTimer]current time: %@, scheduled time: %@, diffrence: %fms", now, scheduledTime, [now timeIntervalSinceDate:scheduledTime]);
    }
}

- (IBAction) triggerDispatchAfter {
    NSDate* now = [NSDate date];
    scheduledTime = [now dateByAddingTimeInterval:10*60*60];
    [self dispatchAfterWithWallTime: scheduledTime];
    [self dispatchAfterWithNonWallTime: scheduledTime];
    NSLog(@"[startDispatchAfter]current time: %@, scheduled time: %@, diffrence: %fms", now, scheduledTime, [now timeIntervalSinceDate:scheduledTime]);
}

- (NSTimer*) startNSTimer:(NSTimeInterval) interval  {
    NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(triggerEventWithNSTimer) userInfo:nil repeats:YES];
    [timer fire];
    return timer;
}

- (void) scheduleEventWithWallTime:(NSDate*) time{
    uint64_t leeway = 5 * NSEC_PER_MSEC;
    wallGCD = [DispatchTimerUtils createDispatchTimer:time withQueue: dispatch_get_main_queue() andLeeway:leeway andBlock: ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            @autoreleasepool {
                [self triggerEventWithWallTime];
            }
        });
    }];
}

- (void) scheduleEventWithNonWallTime:(NSDate*) time{
    uint64_t leeway = 5 * NSEC_PER_MSEC;
    nonwallGCD = [DispatchTimerUtils createDispatchTimerWithNonWallTime:time withQueue: dispatch_get_main_queue() andLeeway:leeway andBlock: ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            @autoreleasepool {
                [self triggerEventWithNonWallTime];
            }
        });
    }];
}

- (void) dispatchAfterWithWallTime:(NSDate*) time{
    [DispatchTimerUtils dispatchAfterWithWallTime:time withQueue: dispatch_get_main_queue() andBlock: ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            @autoreleasepool {
                [self triggerEventWithWallTime];
            }
        });
    }];
}

- (void) dispatchAfterWithNonWallTime:(NSDate*) time{
    [DispatchTimerUtils dispatchAfterWithNonWallTime:time withQueue: dispatch_get_main_queue() andBlock: ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            @autoreleasepool {
                [self triggerEventWithNonWallTime];
            }
        });
    }];
}

- (void)triggerEventWithNSTimer
{
    UIAlertView *eventAlert = [[UIAlertView alloc]
                               initWithTitle:@"DING DING DING" message:@"ns timer meassage!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display the Hello World Message
    [eventAlert show];
    
    NSDate* now = [NSDate date];
    NSLog(@"[NSTimer]current time: %@, scheduled time: %@, diffrence: %fms", now, scheduledTime, [now timeIntervalSinceDate:scheduledTime]);
}


- (void)triggerEventWithWallTime
{
    UIAlertView *eventAlert = [[UIAlertView alloc]
                                    initWithTitle:@"DING DING DING" message:@"wall meassage!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display the Hello World Message
    [eventAlert show];
    
    NSDate* now = [NSDate date];
    NSLog(@"[WALLTIME]current time: %@, scheduled time: %@, diffrence: %fms", now, scheduledTime, [now timeIntervalSinceDate:scheduledTime]);
}

- (void)triggerEventWithNonWallTime
{
    UIAlertView *eventAlert = [[UIAlertView alloc]
                               initWithTitle:@"DING DING DING" message:@"non-wall meassage!!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    // Display the Hello World Message
    [eventAlert show];
    
    NSDate* now = [NSDate date];
    NSLog(@"[NON-WALLTIME]current time: %@, scheduled time: %@, diffrence: %fms", now, scheduledTime, [now timeIntervalSinceDate:scheduledTime]);
}

@end
