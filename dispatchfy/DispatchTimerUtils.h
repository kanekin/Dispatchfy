//
//  DispatchTimerUtils.h
//  dispatchfy
//
//  Created by Tatsuya Kaneko on 07/11/15.
//  Copyright © 2015 Tatsuya Kaneko. All rights reserved.
//

#ifndef DispatchTimerUtils_h
#define DispatchTimerUtils_h


#endif /* DispatchTimerUtils_h */

@interface DispatchTimerUtils : NSObject {
}

+ (dispatch_source_t) createDispatchTimer:(NSDate*) time withQueue: (dispatch_queue_t) queue andLeeway: (uint64_t)leeway andBlock: (void(^)()) block;

+ (dispatch_source_t) createDispatchTimerWithNonWallTime:(NSDate*) time withQueue: (dispatch_queue_t) queue andLeeway: (uint64_t)leeway andBlock: (void(^)()) block;

+ (void) dispatchAfterWithWallTime:(NSDate*) time withQueue: (dispatch_queue_t) queue andBlock: (void(^)()) block;

+ (void) dispatchAfterWithNonWallTime:(NSDate*) time withQueue: (dispatch_queue_t) queue andBlock: (void(^)()) block;

@end